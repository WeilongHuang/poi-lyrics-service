'use strict';
var myApp = angular.module( 'app', [
    'ngSanitize'
    ,'ui.router'
    ,'services'
    ,'filters'
]);

myApp.config([
    '$stateProvider'
    ,'$urlRouterProvider'
    ,function(
        $stateProvider
        ,$urlRouterProvider
    ){
        $stateProvider.state({
            name: 'lyrics'
            ,url: '/lyrics/{songId}'
            ,component: 'lyrics'
            ,resolve: {
                song: function( $stateParams, songService ){
                    return songService( $stateParams.songId );
                }
            }
        });

        $stateProvider.state({
            name: 'songs'
            ,url: '/songs'
            ,component: 'songs'
            ,resolve: {
                songs: function( songListService ){
                    return songListService;
                }
            }
        });

        $urlRouterProvider.otherwise( '/songs' );
}]);
