"use strict";
angular.module( 'filters', [] )
.filter( 'br', [  '$sce', function( $sce ){
        return function( text ){
            return $sce.trustAsHtml(
                text.replace( /\n|&#10;/g, '<br>' )
            );
        }
}]);
