"use strict";

var app = angular.module( 'services', [ 'ui.router' ] )


app.value( 'apiRoot',
    'api/v1/'
);


app.factory( 'songService', [
    '$http'
    ,'$q'
    ,'apiRoot'
    ,function(
        $http
        ,$q
        ,apiRoot
    ){
        return function( songId ){
            var song   = {
                songId: songId
            };

            var lyrics = $http.get(
                apiRoot + 'lyrics/' + songId
                ,{
                    transformResponse: undefined
                }
            ).then( function( response ){
                song.lyrics = response.data.split('\n');
            });

            var metadata = $http.get(
                apiRoot + 'metadata/' + songId
            ).then( function( response ){
                song.metadata = response.data;
            });

            return $q.all(
                    [ lyrics, metadata ]
                ).then( function(){
                    return song;
                });
        }
    }
]);


app.factory( 'songListService', [
    '$http'
    ,'apiRoot'
    ,function(
        $http
        ,apiRoot
    ){
        return $http.get(
            apiRoot + 'songs'
        )
            .then( function( response ){
                return response.data;
            });
    }
]);
