import React, { Component } from 'react';

export default class ViewSignup extends Component {
	render() {
		return (
			<div className="view--signup">
				<div className="pad--m">
					<div className="push-b--s">
						<p className="ts--subheading c--medium-grey ta--center">Sign up for ReadAI</p>
					</div>

					<div className="wrap--xxs push-x--auto ts--small ta--center">
						<input className="w--100 pad--xs  ta--center" type="email" placeholder="E-mail Address" />
						<input className="bdw-t--0 w--100 pad--xs  ta--center" type="password" placeholder="Password" />

						<button className="push-t--s btn btn--cta pad--xs w--100  ts--small fw--600">Register</button>

						<div className="push-t--s  c--grey lc--blue ts--tiny">
							Already Registered? <a href="#" onClick={() => this.props.changeView('LOGIN')}>Login here.</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
