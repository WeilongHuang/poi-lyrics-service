import React, { Component } from 'react';
import HighlightedText from './HighlightedText';

import { RP } from '../helpers';

export default class ViewAnnotate extends Component {
	constructor() {
		super();
		this.state = {
			annotationContent: ''
		}
	}

	postAnnotation(postData) {
		if( postData.content !== '' ) {
			RP.post('annotation', postData).then((res) => {
				console.log(res);
				this.props.focusAnnotation(res.id);
				this.props.changeView('ANNOTATION_DETAIL');
			});
		}
	}

	render() {
		return (
			<div className="view--annotate  pad--s">
				<div className="ts--accent">
					<HighlightedText text={this.props.selectedText} />
				</div>

				<div className="push-t--m">
					<div className="push-b--s">
						<div className="pull-l--s stack-x--xs">
							<div className="va--middle">
								<img className="block radius--100" src={this.props.currentUser.profilePic} width="32" height="32" alt="" />
							</div>
							<div className="va--middle">
								<span className="c--grey">{this.props.currentUser.name}</span>
							</div>
						</div>
					</div>

					<div className="stack-y--xs">
						<div>
							<div className="pull-x--s">
								<textarea className="w--100 ts--body c--medium-grey" rows="4" onChange={(event) => {
									this.setState({ annotationContent: event.target.value })
								}}></textarea>
							</div>
						</div>
						<div>
							<div className="clearfix pull-x--s">
								<div className="float--left">
									<span className="c--grey hover--dim">
										<i className="fa fa-lg fa-paperclip"></i>
										<span className="pad-l--xs  ts--tiny">
											Attach
										</span>
									</span>
								</div>
								<div className="float--right">
									<span className="c--grey hover--dim">
										<i className="fa fa-lg fa-eye-slash"></i>
										<span className="pad-l--xs  ts--tiny">
											Private
										</span>
									</span>
								</div>
							</div>
						</div>
						<div className="pad-t--s">
							<button className={'btn btn--cta w--100 pad-y--xs tt--uppercase ts--tiny ls--slack' + (( this.state.annotationContent === '' ) ? ' is-disabled ' : '' )}
								onClick={() => this.postAnnotation({
									'highlightedText': this.props.selectedText,
									'highlightedTextForIndex': this.props.annotationContext,
									'webUrl': this.props.currentWebUrl,
									'content': this.state.annotationContent,
									'highlightStart': this.props.highlightStart,
									'highlightEnd': this.props.highlightEnd,
									'JSON': this.props.highlightJSON
								})}
							>Annotate</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
