import React, { Component } from 'react';

export default class HighlightedText extends Component {
	render() {
		return (
			<div className="highlight">
				<div className="highlight__text">
					{this.props.text}
				</div>
			</div>
		);
	}
};
