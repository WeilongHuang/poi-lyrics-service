import React, { Component } from 'react';

export default class ViewNotFound extends Component {
	render() {
		return (
			<div className="view--not-found">
				<div className="pad--m stack-y--s">
					<div>
						<p className="ts--accent c--medium-grey ta--center">Whoops, something went wrong!</p>
					</div>
					<div>
						<button onClick={() => this.props.changeView('LOGIN')} className="btn btn--cta w--100 pad-y--xs tt--uppercase ts--tiny ls--slack ta--center">Go Back</button>
					</div>
				</div>
			</div>
		);
	}
};
