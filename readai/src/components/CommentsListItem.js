import React, { Component } from 'react';
import moment from 'moment';

import { RP } from '../helpers';

export default class CommentsListItem extends Component {
	deleteComment(id) {
		if(confirm('Are you sure you want to delete this comment?')) {
			RP.delete('comment', {
				id: id
			}).then((res) => {
				console.log(res);
				this.props.getComments();
			});
		}
	}

	render() {
		const comment = this.props.comment;

		return (
			<div>
				<div className="pad-x--xs pad-b--xs bdc--light-grey bds--solid bdw-b--s  ts--small c--medium-grey">
					<div className="clearfix">
						<div className="float--left">
							<div className="stack-x--xs  push-b--xs">
								<div className="va--middle">
									<img className="block radius--100" src="http://readpeer.com/sites/all/themes/readpeer/images/avatar.png" width="16" height="16" alt="" />
								</div>
								<span className="va--middle  ts--small c--medium-grey fw--600">
									{comment.creatorName}
								</span>
							</div>
						</div>

						<div className="float--right  ts--tiny c--grey">
							{moment(comment.createdTime).format('Do MMM YY')}
						</div>
					</div>

					<div>
						{comment.content}
					</div>

					<div className="clearfix">
						<div className="float--right">
							<span className="ts--tiny fw--600  hover--dim" onClick={() => this.deleteComment(comment.id)}>Delete</span>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
