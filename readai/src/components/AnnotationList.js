import React, { Component } from 'react';

import AnnotationListItem from './AnnotationListItem';

export default class AnnotationList extends Component {
	render() {
		return (
			<div className="stack-y--s">
				{this.props.annotations.map((annotation) => {
					return <AnnotationListItem
								annotation={annotation}
								key={annotation.id}
								currentUser={this.props.currentUser}
								focusAnnotation={this.props.focusAnnotation}
								highlightAnnotations={this.props.highlightAnnotations}
								getAnnotations={this.props.getAnnotations}
								changeView={this.props.changeView}
							/>
				})}
			</div>
		)
	}
}
