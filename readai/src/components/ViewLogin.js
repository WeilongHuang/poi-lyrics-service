import React, { Component } from 'react';

export default class ViewLogin extends Component {
	render() {
		return (
			<div className="view--login">
				<div className="pad--m">
					<div className="push-b--s">
						<p className="ts--subheading c--medium-grey ta--center">Welcome back to ReadAI</p>
					</div>

					<div className="annotation-button">
					</div>

					<div className="wrap--xxs push-x--auto ts--small ta--center">
						<input className="w--100 pad--xs  c--medium-grey" type="email" placeholder="E-mail Address" />
						<input className="bdw-t--0 w--100 pad--xs  c--medium-grey" type="password" placeholder="Password" />

						<button className="push-t--s btn btn--cta pad--xs w--100  ts--small fw--600">Login</button>

						<div className="push-t--s  c--grey lc--blue ts--tiny">
							Don't have an account? <a href="#" onClick={() => this.props.changeView('SIGNUP')}>Sign up.</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
