import React, { Component } from 'react';

export default class RPOverlayToggle extends Component {
	constructor() {
		super();
	}

	render() {
		return (
			<button className="readpeer-overlay-toggle" onClick={this.props.overlayControls().toggle}>
				RP
			</button>
		)
	}
}
