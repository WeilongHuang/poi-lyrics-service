import React, { Component } from 'react';
import HighlightedText from './HighlightedText';
import CommentsList from './CommentsList';
import CommentsReply from './CommentsReply';
import moment from 'moment';

import { RP } from '../helpers';

export default class ViewAnnotationDetail extends Component {
	constructor() {
		super();

		this.state = {
			annotation: null,
			comments: null
		}

		this.getAnnotation     = this.getAnnotation.bind(this);
		this.getComments       = this.getComments.bind(this);
		this.likeAnnotation    = this.likeAnnotation.bind(this);
		this.dislikeAnnotation = this.dislikeAnnotation.bind(this);
	}

	getAnnotation() {
		RP.get('annotation', {
			id: this.props.currentAnnotation
		}).then((data) => {
			this.setState({ annotation: data });
		});
	}

	likeAnnotation() {
		RP.post('like', null, {
			id: this.props.currentAnnotation
		}).then((response) => {
			if(response.code === 1) {
				this.getAnnotation();
			} else {
				alert('Whoops, something went wrong. Error: ' + response.code);
			}
		});
	}

	dislikeAnnotation() {
		RP.post('dislike', null, {
			id: this.props.currentAnnotation
		}).then((response) => {
			if(response.code === 1) {
				this.getAnnotation();
			} else {
				alert('Whoops, something went wrong. Error: ' + response.code);
			}
		});
	}

	getComments() {
		RP.get('comments', {
			id: this.props.currentAnnotation
		}).then((data) => {
			this.setState({ comments: data });
		});
	}

	componentWillMount() {
		this.getAnnotation();
		this.getComments();
	}

	render() {
		while( this.state.annotation === null ) {
			return false;
		}

		return (
			<div className="view--annotation">
				<div className="pad--s  pull-x--s bgc--white">
					<div className="ts--accent">
						<HighlightedText text={this.state.annotation.highlightedText} />
					</div>

					<div className="stack-y--s  push-t--m">
						<div className="clearfix  c--grey">
							<div className="float--left">
								<div className="stack-x--xs">
									<div className="va--middle">
										<img className="block radius--100" src="http://readpeer.com/sites/all/themes/readpeer/images/avatar.png" width="32" height="32" alt="" />
									</div>
									<div className="va--middle  ts--body fw--600 c--medium-grey">
										{this.state.annotation.creatorName}
									</div>
								</div>
							</div>

							<div className="float--right  ts--tiny fw--600">
								{moment(this.state.annotation.createdTime).format('Do MMM YY')}
							</div>
						</div>

						<div className="ts--body c--medium-grey">
							{this.state.annotation.content}
						</div>
					</div>

					<div className="push-t--m  c--grey ts--small">
						<div className="clearfix">
							<div className="float--left">
								<div className="stack-x--xs">
									<span className="c--grey fw--600">
										{this.state.annotation.likeCount - this.state.annotation.dislikeCount}
									</span>
									<i className="c--light-grey  fa fa-lg fa-thumbs-up    hover--dim" onClick={this.likeAnnotation}></i>
									<i className="c--light-grey  fa fa-lg fa-thumbs-down  hover--dim" onClick={this.dislikeAnnotation}></i>
								</div>
							</div>

							<div className="float--right">
								<div className="stack-x--xs  hover--dim">
									<div className="c--grey fw--600">
										Share
									</div>
									<div>
										<i className="c--light-grey  fa fa-lg fa-share-alt"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<section className="push-t--s  pad-x--xs">
					<CommentsReply currentAnnotation={this.props.currentAnnotation} commentCount={this.state.comments.array.length} getComments={this.getComments} />
					<CommentsList comments={this.state.comments} getComments={this.getComments} />
				</section>
			</div>
		)
	}
}
