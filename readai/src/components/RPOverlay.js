import React, { Component } from 'react';
import Frame from 'react-frame-component';
import OverlayHeader from './OverlayHeader';
import OverlayBody from './OverlayBody';

export default class RPOverlay extends Component {
	constructor() {
		super();
	}

	componentDidMount() {
		document.onkeydown = (event) => {
			if(event.key === 'Escape') {
				this.props.overlayControls().toggle();
			}
		}
	}

	render() {
		return (
			<Frame
				id="readpeer-overlay"
				className={'readpeer-overlay  bgc--xlight-grey ' + (this.props.isActive ? 'is-active' : '')}
				head={
					<head>
						<link type="text/css" rel="stylesheet" href="/style.min.css" />
						<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" />
						<link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css" integrity="sha384-Wrgq82RsEean5tP3NK3zWAemiNEXofJsTwTyHmNb/iL3dP/sZJ4+7sOld1uqYJtE" crossOrigin="anonymous" />
					</head>
				}
			>

				<OverlayHeader 
					currentView={this.props.currentView}
					changeView={this.props.changeView}
					overlayControls={this.props.overlayControls}
				/>
				
				<OverlayBody
					currentUser={this.props.currentUser}
					currentWebUrl={this.props.currentWebUrl}
					currentView={this.props.currentView}
					currentAnnotation={this.props.currentAnnotation}
					changeView={this.props.changeView}
					focusAnnotation={this.props.focusAnnotation}
					getAnnotations={this.props.getAnnotations}
					highlightAnnotations={this.props.highlightAnnotations}
					annotations={this.props.annotations}
					selectedText={this.props.selectedText}
					annotationContext={this.props.annotationContext}
					highlightStart={this.props.highlightStart}
					highlightEnd={this.props.highlightEnd}
					highlightJSON={this.props.highlightJSON}
				/>
			</Frame>
		)
	}
}
