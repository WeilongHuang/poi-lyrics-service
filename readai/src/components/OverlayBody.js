import React, { Component } from 'react';

import ViewAnnotate from './ViewAnnotate';
import ViewAnnotationFeed from './ViewAnnotationFeed';
import ViewAnnotationDetail from './ViewAnnotationDetail';
import ViewLogin from './ViewLogin';
import ViewSignup from './ViewSignup';
import ViewNotFound from './ViewNotFound';

export default class OverlayBody extends Component {
	render() {
		let View = null;
		switch(this.props.currentView) {
			case 'ANNOTATION_FEED':
				View = <ViewAnnotationFeed
							currentUser={this.props.currentUser}
							currentWebUrl={this.props.currentWebUrl}
							changeView={this.props.changeView}
							focusAnnotation={this.props.focusAnnotation}
							getAnnotations={this.props.getAnnotations}
							highlightAnnotations={this.props.highlightAnnotations}
							annotations={this.props.annotations}
						/>;
				break;
			case 'ANNOTATION_DETAIL':
				View = <ViewAnnotationDetail
							currentUser={this.props.currentUser}
							currentAnnotation={this.props.currentAnnotation}
							changeView={this.props.changeView}
						/>;
				break;
			case 'ANNOTATE':
				View = <ViewAnnotate
							currentUser={this.props.currentUser}
							currentWebUrl={this.props.currentWebUrl}
							changeView={this.props.changeView}
							focusAnnotation={this.props.focusAnnotation}
							selectedText={this.props.selectedText}
							annotationContext={this.props.annotationContext}
							highlightStart={this.props.highlightStart}
							highlightEnd={this.props.highlightEnd}
							highlightJSON={this.props.highlightJSON}
						/>;
				break;
			case 'LOGIN':
				View = <ViewLogin
							currentUser={this.props.currentUser}
							changeView={this.props.changeView}
						/>;
				break;
			case 'SIGNUP':
				View = <ViewSignup
							currentUser={this.props.currentUser}
							changeView={this.props.changeView}
						/>;
				break;
			default:
				View = <ViewNotFound
							changeView={this.props.changeView} 
						/>;
		}

		return (
			<div className="readpeer-overlay__body  pad--s pad-b--m">
				{View}
			</div>
		);
	}
};
