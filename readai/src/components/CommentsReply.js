import React, { Component } from 'react';

import { RP } from '../helpers';

export default class CommentsReply extends Component {
	constructor() {
		super();
		this.state = {
			commentContent: ''
		}
	}

	postComment(postData) {
		if( postData.content !== '' ) {
			RP.post('comment', postData, {
				id: this.props.currentAnnotation
			}).then((res) => {
				console.log(res);
				if(res.code === 1) {
					console.log('refreshing...');
					this.props.getComments();
				}
			});
		}
	}

	render() {
		return (
			<section>
				<div className="push-b--xs">
					<label>
						<div className="ts--tiny c--grey fw--600">Leave a comment</div>
						<textarea className="w--100 ts--small c--medium-grey" rows="2" onChange={(event) => {
							this.setState({ commentContent: event.target.value })
						}}></textarea>
					</label>
				</div>

				<div className="push-b--s  ts--accent c--grey">
					<div className="clearfix">
						<div className="float--left  push-t--xs">
							{(this.props.commentCount > 0) ?
							  this.props.commentCount + ' comments'
							: 'No comments yet' }
						</div>

						<div className="float--right">
							<button className={'btn fs--xtiny pad-x--s pad-y--xs bgc--dark-grey c--white tt--uppercase ls--slack hover--dim' + (( this.state.commentContent === '' ) ? ' is-disabled ' : '' )}
							onClick={() => this.postComment({
								'content': this.state.commentContent
							})}>Reply</button>
						</div>
					</div>
				</div>
			</section>
		)
	}
}
