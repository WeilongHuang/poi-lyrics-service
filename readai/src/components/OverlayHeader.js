import React, { Component } from 'react';

export default class OverlayHeader extends Component {
	render() {
		let backButtonClass = 'hidden';
		let backButtonHandler = null;

		switch(this.props.currentView) {
			case 'ANNOTATE' :
				backButtonClass = '';
				backButtonHandler = () => {
					this.props.changeView('ANNOTATION_FEED');
				}
				break;
			case 'ANNOTATION_DETAIL' :
				backButtonClass = '';
				backButtonHandler = () => {
					this.props.changeView('ANNOTATION_FEED');
				}
				break;
			default :
				break;
		}

		return (
			<header className="readpeer-overlay__header  relative">
		 		<div className="absolute t--0 l--0  pad-y--xs pad-l--xs">
		 			<div className={'hover--dim ' + backButtonClass} onClick={backButtonHandler}>
						<button className="fa fa-lg fa-chevron-left  c--light-grey"></button>
						<span className="ts--small c--light-grey"> Back</span>
					</div>
				</div>

		 		<div className="absolute t--0 r--0  pad-y--xs pad-r--xs">
		 			<div className="stack-x--s">
						<div>
							<button className="fa fa-lg fa-times  c--light-grey  hover--dim" onClick={this.props.overlayControls().close}></button>
						</div>
					</div>
				</div>

				<h1 className="push-t--xs  ta--center">
					<span className="c--grey  hover--dim pull-r--s">
						<span className="push-r--xs  va--middle  ts--subheading fw--300">
							ReadAI
						</span>
						<i className="va--middle  fa fa-chevron-down ts--tiny c--light-grey"></i>
					</span>
				</h1>
			</header>
		);
	}
};
