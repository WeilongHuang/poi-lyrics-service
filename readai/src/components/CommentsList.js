import React, { Component } from 'react';

import CommentsListItem from './CommentsListItem';

export default class CommentsList extends Component {
	render() {
		const comments = this.props.comments.array;

		return (
			<div className="stack-y--s">
				{comments.map((comment) => {
					return <CommentsListItem comment={comment} key={comment.id} getComments={this.props.getComments} />
				})}
			</div>
		)
	}
}
