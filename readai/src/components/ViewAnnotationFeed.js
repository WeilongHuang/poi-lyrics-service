import React, { Component } from 'react';
import AnnotationList from './AnnotationList';

// import { RP } from '../helpers';

export default class ViewAnnotationFeed extends Component {
	constructor() {
		super();
	}

	componentWillMount() {
		this.props.getAnnotations();
	}

	render() {
		if ( this.props.annotations.length > 0 ) {
			return (
				<div className="app-view-annotation-list">
					<header className="push-b--s pull-x--s pad-y--xs pad-x--s pad-t--0  bdc--xlight-grey bds--solid bdw-b--s">
						<div className="clearfix">
							<div className="hidden float--left">
								<select name="filter-annotations" className="bds--none  ts--small c--grey  hover--dim">
									<option value="all">All Annotations</option>
									<option value="my">My Annotations</option>
								</select>
							</div>

							<div className="ta--center fw--600 c--medium-grey"
								onClick={() => {
									this.props.highlightAnnotations(this.props.annotations);
								}
							}>
								{this.props.annotations.length} Annotations
							</div>
						</div>
					</header>

					<AnnotationList
						annotations={this.props.annotations}
						currentUser={this.props.currentUser}
						changeView={this.props.changeView}
						focusAnnotation={this.props.focusAnnotation}
						highlightAnnotations={this.props.highlightAnnotations}
						getAnnotations={this.props.getAnnotations}
					/>
				</div>
			);
		} else {
			return (
				<div className="app-view-annotation-list--empty  pad--s">
					<p className="ts--accent c--grey ta--center">There are no annotations here yet!</p>
				</div>
			)
		}
	}
};
