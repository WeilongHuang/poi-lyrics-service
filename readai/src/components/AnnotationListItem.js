import React, { Component } from 'react';
import moment from 'moment';

import { RP } from '../helpers';

import HighlightedText from './HighlightedText';

export default class AnnotationListItem extends Component {
	constructor() {
		super();

		this.goToAnnotation    = this.goToAnnotation.bind(this);
		this.deleteAnnotation  = this.deleteAnnotation.bind(this);
		this.likeAnnotation    = this.likeAnnotation.bind(this);
		this.dislikeAnnotation = this.dislikeAnnotation.bind(this);
	}

	goToAnnotation() {
		this.props.focusAnnotation(this.props.annotation.id);
		this.props.changeView('ANNOTATION_DETAIL');
	}

	likeAnnotation() {
		RP.post('like', null, {
			id: this.props.annotation.id
		}).then((response) => {
			if(response.code === 1) {
				this.props.getAnnotations();
			} else {
				alert('Whoops, something went wrong. Error: ' + response.code);
			}
		});
	}

	dislikeAnnotation() {
		RP.post('dislike', null, {
			id: this.props.annotation.id
		}).then((response) => {
			if(response.code === 1) {
				this.props.getAnnotations();
			} else {
				alert('Whoops, something went wrong. Error: ' + response.code);
			}
		});
	}

	deleteAnnotation() {
		if(confirm('Are you sure you want to delete this annotation?')) {
			RP.delete('annotation', {
				id: this.props.annotation.id
			}).then((response) => {
				if(response.code === 1) {
					this.props.getAnnotations();
				} else {
					alert('Whoops, something went wrong. Error: ' + response.code);
				}
			});
		}
	}

	render() {
		const annotation = this.props.annotation;

		return (
			<article className="pad--s pad-b--0 bgc--white c--medium-grey shadow--light radius--s"
				onMouseEnter={() => {
					const annotations = [ annotation ];
					this.props.highlightAnnotations(annotations)
				}}
			>
				<div className="push-b--s  ts--body">
					<span onClick={this.goToAnnotation} className="block hover--dim">
						<HighlightedText text={annotation.highlightedText} />
					</span>
				</div>

				<div className="clearfix  push-b--xs  ts--tiny c--grey">
					<div className="float--left">
						<div className="stack-x--xs">
							<div className="va--middle">
								<img className="block radius--100" src="http://readpeer.com/sites/all/themes/readpeer/images/avatar.png" width="24" height="24" alt="{annotation.creatorName}" />
							</div>
							<div className="va--middle  ts--small fw--600 c--medium-grey">
								{annotation.creatorName}
							</div>
						</div>
					</div>

					<div className="float--right">
						<span className="block hover--dim  fw--600" onClick={this.goToAnnotation}>{moment(annotation.createdTime).format('Do MMM YY')}</span>
					</div>
				</div>

				<div className="ts--small c--medium-grey">
					{annotation.content}
				</div>

				<footer className="push-t--s pull-x--s pad-x--s pad-y--xs  bdc--xlight-grey bds--solid bdw-t--s  c--light-grey ts--tiny">
					<div className="clearfix">
						<div className="float--left">
							<div className="stack-x--xs">
								<div className="stack-x--xs">
									<span className="c--grey fw--600">{annotation.likeCount - annotation.dislikeCount}</span>
									<i className="c--light-grey  fa fa-lg fa-thumbs-up    hover--dim" onClick={this.likeAnnotation}></i>
									<i className="c--light-grey  fa fa-lg fa-thumbs-down  hover--dim" onClick={this.dislikeAnnotation}></i>
								</div>
								<div className="pad-l--xs">
									<span onClick={this.goToAnnotation} className="hover--dim  ts--xtiny c--grey fw--600">
										{(annotation.commentCount > 0) ? annotation.commentCount + ' comments' : 'Comment' }
									</span>
								</div>
								{(annotation.creatorId === this.props.currentUser.id) ? (
								<div className="stack-x--xs  pad-l--xs  ts--xtiny c--grey lc--grey fw--600">
									<span className="hover--dim" onClick={this.deleteAnnotation}>Delete</span>
								</div>
								) : null}
							</div>
						</div>

						<div className="float--right">
							<i className="c--light-grey  fa fa-lg fa-share-alt  hover--dim"></i>
						</div>
					</div>
				</footer>
			</article>
		)
	}
}
