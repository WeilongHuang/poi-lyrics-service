import React, { Component } from 'react';
import Parser from 'html-to-json';

import { RP } from '../helpers';

export default class RPWebContent extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			pageBody: null,
			pageHead: null
		}
	}

	componentWillMount() {
		const currentWebUrl = this.props.location.pathname.slice(1);
		this.props.updateUrl(currentWebUrl);

		RP.get('webpage', {
			url: currentWebUrl
		}).then((data) => {
			const $head = document.head || document.getElementsByTagName("head")[0];
			const $body = document.body || document.getElementsByTagName("body")[0];

			Parser.parse(data.pageHead, {
				'head': function($doc) {
					return $doc.find('head').attr();
				}
			}, function(err, result) {
				const head = result.head;
				for(var attr in head) {
					$head.setAttribute(attr, head[attr]);
				}
			});

			Parser.parse(data.pageBody, {
				'body': function($doc) {
					return $doc.find('body').attr();
				}
			}, function(err, result) {
				const body = result.body;
				for(var attr in body) {
					$body.setAttribute(attr, body[attr]);
				}
			});

			$head.innerHTML += data.pageHead;

			this.setState({
				pageHead: data.pageHead,
				pageBody: data.pageBody
			})
		})
	}

	render() {
		return (
			<div
				id="readpeer-web-content"
				onMouseUp={this.props.handleSelection}
				onTouchEnd={this.props.handleSelection}
				dangerouslySetInnerHTML={{__html: this.state.pageBody}}
			>
			</div>
		)
	}
}
