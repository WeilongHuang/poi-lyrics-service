import React, { Component } from 'react';
import Parser from 'html-to-json';
import _ from 'lodash';
import cheerio from 'cheerio';

import { RP } from '../helpers';

export default class RPWebContent extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			pageBody: null,
			headScripts: null,
			bodyScripts: null,
			pageScriptsLoaded: false
		}
	}

	componentWillMount() {
		const $this = this;
		const currentWebUrl = this.props.location.pathname.slice(1);
		this.props.updateUrl(currentWebUrl);
		const $html = document.documentElement || document.getElementsByTagName("html")[0];
		const $head = document.head || document.getElementsByTagName("head")[0];
		const $body = document.body || document.getElementsByTagName("body")[0];

		RP.get('webpage', {
			url: currentWebUrl
		}).then((data) => {
			var bodyContent = '';

			Parser.parse(data.pageContent, {
				'htmlAttrs': function($doc) {
					return $doc.find('html').attr();
				},
				'headAttrs': function($doc) {
					return $doc.find('head').attr();
				},
				'bodyAttrs': function($doc) {
					return $doc.find('body').attr();
				},
				'head': function($doc) {
					return $doc.find('head').html();
				},
				'body': function($doc) {
					return $doc.find('body').html();
				},
				'headScripts': function($doc) {
					return $doc.find('head script');
				},
				'bodyScripts': function($doc) {
					return $doc.find('body script');
				}
			}, function(err, result) {
				// Set state for <head> <script>s
				var headScripts = [];
				_.times(result.headScripts.length, function(i) {
					const script = result.headScripts[i];
					const attrs = script.attribs;
					const content = script.children[0];

					var $s = document.createElement('script');

					// Set script attributes
					for(var attr in attrs) {
						const val = script.attribs[attr];
						$s.setAttribute(attr, val);
					}

					if(!$s.getAttribute('async')) {
						$s.async = false;
					}

					// Set innerHTML for inline scripts
					if(content) {
						$s.innerHTML = content.data
					}

					// Add script to array
					headScripts.push($s);
				});

				$this.setState({
					headScripts
				});

				// Set state for <body> <script>s
				var bodyScripts = [];
				_.times(result.bodyScripts.length, function(i) {
					const script = result.bodyScripts[i];
					const attrs = script.attribs;
					const content = script.children[0];

					var $s = document.createElement('script');

					// Set script attributes
					for(var attr in attrs) {
						const val = script.attribs[attr];
						$s.setAttribute(attr, val);
					}

					if(!$s.getAttribute('async')) {
						$s.async = false;
					}

					// Set innerHTML for inline scripts
					if(content) {
						$s.innerHTML = content.data
					}

					// Add script to array
					bodyScripts.push($s);
				});

				$this.setState({
					bodyScripts
				});

				// Set <html> attributes
				for(var attr in result.htmlAttrs) {
					const val = result.htmlAttrs[attr];
					$html.setAttribute(attr, val);
				}

				// Set <head> attributes
				for(var attr in result.headAttrs) {
					const val = result.headAttrs[attr];
					$head.setAttribute(attr, val);
				}

				// Set <body> attributes
				for(var attr in result.bodyAttrs) {
					const val = result.bodyAttrs[attr];
					$body.setAttribute(attr, val);
				}

				// Set <head> content
				cheerio.parseHTML(result.head).forEach((node)=>{
					$head.innerHTML += cheerio.load(node).html();
				});

				// Set state for <body>
				var bodyState = [];
				cheerio.parseHTML(result.body).forEach((node)=>{
					bodyState.push(cheerio.load(node).html());
				});

				$this.setState({
					pageBody: bodyState.join('')
				});
			});
		});
	}

	componentDidUpdate() {
		const $head = document.head || document.getElementsByTagName("head")[0];
		const $body = document.body || document.getElementsByTagName("body")[0];

		if (
			this.state.pageBody !== null &&
			this.state.headScripts !== null &&
			this.state.bodyScripts !== null &&
			this.state.pageScriptsLoaded === false
		) {
			///
			// Parse and append scripts to page
			//
			this.state.headScripts.forEach((script) => {
				$head.appendChild(script);
			});

			this.state.bodyScripts.forEach((script) => {
				$body.appendChild(script);
			});

			this.setState({
				pageScriptsLoaded: true
			});
		}
	}

	render() {
		return (
			<div
				id="readpeer-web-content"
				onMouseUp={this.props.handleSelection}
				onTouchEnd={this.props.handleSelection}
				dangerouslySetInnerHTML={{__html: this.state.pageBody}}
			>
			</div>
		)
	}
}
