import React, { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Match } from 'react-router';

import { RP } from './helpers';

import rangy from 'rangy';
import rangyHighlight from 'rangy/lib/rangy-highlighter';
import rangyClassApplier from 'rangy/lib/rangy-classapplier';
import rangyTextRange from 'rangy/lib/rangy-textrange';
import rangySerializer from 'rangy/lib/rangy-serializer';

import RPWebContent from './components/RPWebContent';
import RPOverlay from './components/RPOverlay';
import RPOverlayToggle from './components/RPOverlayToggle';

require('../public/readpeer-overlay.css');

class App extends Component {
	constructor() {
		super();

		this.state = {
			isActive: false,
			currentUser: {
				id: 0,
				name: 'anonymous',
				profilePic: 'http://readpeer.com/sites/all/themes/readpeer/images/avatar.png'
			},
			currentWebUrl: "",
			currentView: 'ANNOTATION_FEED',
			annotations: [],
			currentAnnotation: null,
			selectedText: null
		}

		this.overlayControls = this.overlayControls.bind(this);
		this.updateUrl       = this.updateUrl.bind(this);
		this.changeView      = this.changeView.bind(this);
		this.focusAnnotation = this.focusAnnotation.bind(this);
		this.handleSelection = this.handleSelection.bind(this);
		this.getAnnotations  = this.getAnnotations.bind(this);
		this.highlightAnnotations  = this.highlightAnnotations.bind(this);
	}

	componentDidMount() {
		// this.getAnnotations();
	}

	overlayControls() {
		return {
			open: () => {
				this.setState({ isActive: true });
			},
			close: () => {
				this.setState({ isActive: false });
			},
			toggle: () => {
				this.setState({ isActive: !this.state.isActive });
			}
		}
	}

	updateUrl(url) {
		return this.setState({ currentWebUrl: url });
	}

	changeView(view) {
		return this.setState({ currentView: view });
	}

	focusAnnotation(id) {
		return this.setState({ currentAnnotation: id });
	}

	getAnnotations() {
		// (this.props.currentWebUrl) doesnt work due to a race condition:
		// https://facebook.github.io/react/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous
		// use window.location.pathname instead.

		RP.get('annotations', {
			url: window.location.pathname.slice(1)
		}).then((data) => {
			this.setState({ annotations: data.array });
		});
	}

	highlightAnnotations(annotations) {
		///
		// Highlight annotations in RPWebContent
		//

		const className = 'readpeer-highlight';
		const highlightClassApplier = rangy.createClassApplier(className, {
			tagNames: [ 'span', 'a', 'b' ]
		});

		const highlighter = rangy.createHighlighter(document, 'TextRange');
		highlighter.addClassApplier(highlightClassApplier);

		// Remove existing highlights first
		Array.prototype.slice.call(document.querySelectorAll('.'+className)).forEach((el) => {
			el.classList.remove(className);
		});

		// Highlight all annotations
		var selectionRanges = [];
		var selection = rangy.getSelection();

		annotations.forEach((annotation) => {
			if(annotation.highlightEnd > -1) {
				const characterRange = {
					'start': annotation.highlightStart,
					'end':   annotation.highlightEnd
				}

				selectionRanges.push({
					characterRange
				});
			} else {
				// const range = rangy.createRange();
				// range.selectNodeContents(document.getElementById('readpeer-web-content'));

				// console.log(annotation);
				// console.log(range.findText(annotation.highlightedText));
				// console.log(range.findText(annotation.highlightedTextForIndex));
			}

			selection.restoreCharacterRanges(document, selectionRanges);
			highlighter.highlightSelection(className, selection);
		});

		selection.removeAllRanges();
	}

	handleSelection() {
		const className = 'readpeer-selection';
		const selectionClassApplier = rangy.createClassApplier(className, {
			tagNames: [ 'span', 'a', 'b' ]
		});

		var selection         = rangy.getSelection();
		var selectionRange    = selection.getRangeAt(0).toCharacterRange(document.body);
		var selectedText      = selection.getRangeAt(0).toString();
		var annotationContext = selection.getRangeAt(0).toHtml();
		var highlightJSON     = rangy.serializeSelection(selection, true);

		if(selectedText !== '') {
			// Remove existing highlights first
			Array.prototype.slice.call(document.querySelectorAll('.readpeer-highlight')).forEach((el) => {
				el.classList.remove('readpeer-highlight');
			});

			// Remove previous annotation buttons
			var $annotationButtons = document.querySelectorAll('#readpeer-annotation-button');
			Array.prototype.slice.call($annotationButtons).forEach((el) => {
				el.parentNode.removeChild(el);
			});

			// Remove className from previous selections
			var $selectedEls = Array.prototype.slice.call( document.getElementsByClassName(className), 0 );
			$selectedEls.forEach((el) => {
				el.classList.remove(className);
			});

			// Apply className to current selection
			selectionClassApplier.toggleSelection();

			// Create annotation button
			var annotationButton = document.createElement('button');
			annotationButton.id = 'readpeer-annotation-button';

			// Append annotation button
			var $currentSelection = Array.prototype.slice.call( document.getElementsByClassName(className), 0 )[0];
			$currentSelection.appendChild(annotationButton);

			// Annotation button event listener
			var $annotationButton = document.getElementById('readpeer-annotation-button');
			$annotationButton.addEventListener('mouseup', () => {
				selection.removeAllRanges();
				this.setState({
					isActive: true,
					selectedText: selectedText,
					annotationContext: annotationContext,
					highlightStart: selectionRange.start,
					highlightEnd: selectionRange.end,
					highlightJSON: highlightJSON
				});

				this.changeView('ANNOTATE');
			});

			// Clear ranges
			selection.removeAllRanges();
		}
	}

	render() {
		return (
			<div id="readpeer">

				<BrowserRouter>
					<Match pattern="*" render={(props) => (
						<RPWebContent
							overlayControls={this.overlayControls}
							handleSelection={this.handleSelection}
							updateUrl={this.updateUrl}
							{...props}
						/>
					)} />
				</BrowserRouter>

				<RPOverlayToggle
					isActive={this.state.isActive}
					overlayControls={this.overlayControls}
				/>

				<RPOverlay
					isActive={this.state.isActive}
					overlayControls={this.overlayControls}
					currentView={this.state.currentView}
					currentUser={this.state.currentUser}
					currentWebUrl={this.state.currentWebUrl}
					currentAnnotation={this.state.currentAnnotation}
					selectedText={this.state.selectedText}
					annotationContext={this.state.annotationContext}
					highlightStart={this.state.highlightStart}
					highlightEnd={this.state.highlightEnd}
					highlightJSON={this.state.highlightJSON}
					changeView={this.changeView}
					focusAnnotation={this.focusAnnotation}
					getAnnotations={this.getAnnotations}
					highlightAnnotations={this.highlightAnnotations}
					annotations={this.state.annotations}
				/>
			</div>
		)
	}
}

render(
	<App />,
	document.querySelector('body')
);
