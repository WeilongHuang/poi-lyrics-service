export const RP = {
	get: (resource, params) => {
		let urlString = '';
		if(params.url === null || params.url === "")
			params.url = "http://st.poi.life/static/lyrics/";
		switch(resource) {
			case 'webpage' :
				urlString = '/api/web/v2?url=' + params.url;
				break;
			case 'annotations' :
				urlString = '/api/annotation/list/v2?webUrl=' + params.url;
				break;
			case 'annotation' :
				urlString = '/api/annotation/' + params.id;
				break;
			case 'comments' :
				urlString = '/api/annotation/comments/' + params.id;
				break;
			default:
				return false;
		}

		if( urlString !== '' ) {
			return new Promise((resolve, reject) => {
				fetch(urlString)
				.then((response) => {
					if (response.status !== 200) {
						console.log('Looks like there was a problem. Status Code: ' +  response.status);
						return;
					}

					response.json().then((data) => {					
						if(data.code < 0) {
							reject(new Error('You have entered an invalid URL'));
						}
						
						resolve(data);
					});
				})
				.catch((err) => {
					console.log('FETCH ERROR: ', err);
				});
			});
		}
	},

	post: (resource, postData, params) => {
		let urlString = '';

		switch(resource) {
			case 'annotation' :
				urlString = '/api/annotation/save';
				break;
			case 'comment' :
				urlString = '/api/annotation/comment/' + params.id;
				break;
			case 'like' :
				urlString = '/api/annotation/like/' + params.id;
				break;
			case 'dislike' :
				urlString = '/api/annotation/dislike/' + params.id;
				break;
			default:
				return false;
		}

		if( urlString !== '' ) {
			return new Promise((resolve, reject) => {
				var options = {
					method: 'POST'
				};

				if(postData) {
					options.headers = {
						'Content-Type': 'application/json'
					};
					options.body = JSON.stringify(postData);
				}

				fetch(urlString, options)
				.then((response) => {
					if (response.status !== 200) {
						console.log('Looks like there was a problem. Status Code: ' +  response.status);
						return;
					}

					response.json().then((data) => {					
						if(data.code < 0) {
							reject(new Error('ERROR CODE ' + data.code));
						}
						
						resolve(data);
					});
				})
				.catch((err) => {
					console.log('FETCH ERROR: ', err);
				});
			});
		}
	},

	delete: (resource, params) => {
		let urlString = '';

		switch(resource) {
			case 'annotation' :
				urlString = '/api/annotation/' + params.id;
				break;
			case 'comment' :
				urlString = '/api/comment/' + params.id;
				break;
			default:
				return false;
		}

		if( urlString !== '' ) {
			return new Promise((resolve, reject) => {
				fetch(urlString, {
					method: 'DELETE'
				})
				.then((response) => {
					if (response.status !== 200) {
						console.log('Looks like there was a problem. Status Code: ' +  response.status);
						return;
					}

					response.json().then((data) => {					
						if(data.code < 0) {
							reject(new Error('You have entered an invalid URL'));
						}
						
						resolve(data);
					});
				})
				.catch((err) => {
					console.log('FETCH ERROR: ', err);
				});
			});
		}
	}
}
